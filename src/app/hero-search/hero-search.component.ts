import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import {
	debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
	selector: 'app-hero-search',
	templateUrl: './hero-search.component.html',
	styleUrls: ['./hero-search.component.scss']
})
export class HeroSearchComponent implements OnInit {

	/** From the Docs: A Subject is both a source of observable values and an Observable itself. You can subscribe to a Subject as you would any Observable.
		You can also push values into that Observable by calling its next(value) method as the search() method does. */

	heroes$: Observable<Hero[]>;
	private searchTerms = new Subject<string>();

	constructor(private heroService: HeroService) { }

	// Push a search term into the observable stream
	search(term: string): void {
		this.searchTerms.next(term);
	}

	ngOnInit() {
		this.heroes$ = this.searchTerms.pipe(
			// wait 300ms after each keystroke
			debounceTime(300),

			// ignore new term until it changes
			distinctUntilChanged(),

			// and then switch to new observable, every time the term changes.
			switchMap((term: string) => this.heroService.searchHeroes(term)),
		);
	}
}
