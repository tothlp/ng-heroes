import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
	selector: 'app-heroes',
	templateUrl: './heroes.component.html',
	styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

	// Create a component property to hold heroes..
	// We will use it in the template directly.
	heroes: Hero[];

	constructor(private heroService: HeroService) {
		// During creating a HeroesComponent, Angular sets the heroService parameter to the singleton instance of HeroService.

	}

	/**
	 * Method for setting the value of heroes component property to the returned array from heroService.
	 */
	getHeroes(): void {
		this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes);
	}

	add(name: string): void {
		name = name.trim();
		if (!name) { return; }
		this.heroService.addHero({ name } as Hero).subscribe(hero => { this.heroes.push(hero) });
	}

	delete(hero: Hero): void {
		// First, we need to remove our hero from the heroes list, then we can ... kill him.
		this.heroes = this.heroes.filter(h => h != hero);
		this.heroService.deleteHero(hero).subscribe();
	}

	ngOnInit() {
		this.getHeroes();
	}

}
